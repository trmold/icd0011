package test.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import util.FileUtil;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class OrderDao {

    @Autowired
    private JdbcTemplate jdbctemplate;

    public OrderDao(JdbcTemplate template) {
        this.jdbctemplate = jdbctemplate;
    }

    @PostConstruct
    public void createSchema(){

        String sql1 = FileUtil.readFileFromClasspath("schema.sql");
        String sql2 = FileUtil.readFileFromClasspath("data.sql");

        jdbctemplate.update(sql1);
        jdbctemplate.update(sql2);

    }

    public List<Order> findOrders() {

        String sql = "select id, ordernumber from orders";

        return jdbctemplate.query(
                sql,
                (rs, rowNum) ->
                        new Order(
                                rs.getLong("id"),
                                rs.getString("ordernumber"),
                                findOrdersRowsByOrderId(rs.getLong("id"))
                        )
        );

    }

    public Order findOrderById(Long id) {

        String sql = "select id, ordernumber from orders where id = ?";
        Order order = (Order)jdbctemplate.queryForObject (sql, new Object[]{id}, new BeanPropertyRowMapper(Order.class));
        order.setOrderRows(findOrdersRowsByOrderId(id));
        return order;
    }

    public List<OrderRow> findOrdersRowsByOrderId(Long orderid) {

        String sql = "select id, orderid, itemname, quantity, price from orderrows where orderid = ?";
        return jdbctemplate.query(sql, new Object[]{orderid}, new BeanPropertyRowMapper<>(OrderRow.class));
    }

    public Order insertOrder(Order order) {

        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbctemplate);
        jdbcInsert.withTableName("orders").usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("ordernumber", order.getorderNumber());
        // execute insert
        Long neworderId = (Long)jdbcInsert.executeAndReturnKey(new MapSqlParameterSource(parameters));

        //Write orderrows to orderrows table
        if (order.getOrderRows() != null){
            List<OrderRow> myOrderRows = order.getOrderRows();
            List<OrderRow> myOrderRowswid = new ArrayList<>();

            //Add orderID to each order row and save to the orderrow table
            for (OrderRow item : myOrderRows) {
                OrderRow orderrowwid = insertOrderRow(item, neworderId);
                myOrderRowswid.add(orderrowwid);
            }
            order.setOrderRows(myOrderRowswid);
        }

        order.setId(neworderId);
        return order;
    }

    public OrderRow insertOrderRow(OrderRow orderrow, Long orderid) {

        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbctemplate);
        jdbcInsert.withTableName("orderrows").usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("orderid", orderid);
        parameters.put("itemname", orderrow.getItemName());
        parameters.put("quantity", orderrow.getQuantity());
        parameters.put("price", orderrow.getPrice());
        // execute insert
        Long neworderrowsId = (Long)jdbcInsert.executeAndReturnKey(new MapSqlParameterSource(parameters));

        orderrow.setId(neworderrowsId);

        return orderrow;
    }

    public void deleteOrder(Long id) {

        String sql = "Delete from orders where id = ?;\n" +
                "Delete from orderrows where orderid = ?";

        Object[] args = {id, id};
        jdbctemplate.update(sql, args);

    }
}
