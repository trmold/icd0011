package test.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderRow {
    private Long id;
    private Long orderId;
    private String itemName;

    @NotNull
    @Min(1)
    private Long quantity;

    @NotNull
    @Min(1)
    private Long price;

    @JsonIgnore
    public Long getId() {     return id;}

    public void setId(Long id) {     this.id = id;}

    @JsonIgnore
    public Long getOrderId() { return orderId; }

    public void setOrderNumber(Long orderId) {
        this.orderId = orderId;
    }

    public String getItemName() { return itemName; }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Long getQuantity() { return quantity; }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getPrice() { return price; }

    public void setPrice(Long price) { this.price = price; }

    @Override
    public String toString() {
        return "{" +
                "itemName='" + itemName + '\'' +
                ", \"quantity=" + quantity +
                ", \"price=" + price +
                '}';
    }
}
