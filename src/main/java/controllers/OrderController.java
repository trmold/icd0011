package controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import test.model.Order;
import test.model.OrderDao;
import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderDao dao;

    @GetMapping("orders")
    public List<Order> getOrders() {
        return dao.findOrders();
    }

    @GetMapping("orders/{id}")
    public Order getOrder(@PathVariable Long id) {
        return dao.findOrderById(id);
    }

    @PostMapping("orders")
    public Order saveOrder(@RequestBody @Valid Order order) {
        return dao.insertOrder(order);
    }

    @DeleteMapping ("orders/{id}")
    public void deleteOrder(@PathVariable Long id) {
        dao.deleteOrder(id);
    }

}